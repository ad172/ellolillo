using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rayCastCamara : MonoBehaviour
{
    //bool desplazarse = false;
    Vector3 destino;
    Vector3 origen;
    Vector3[] Direccion;
    float distancia = 1f;

    private void Start()
    {
        destino = transform.position;
        Direccion = new Vector3[4];
        Direccion[0] = Vector3.forward;
        Direccion[1] = Vector3.back;
        Direccion[2] = Vector3.right;
        Direccion[3] = Vector3.left;
    }

    void Update()
    {
        ClickRaycast();
        Desplazarse();
    }

    void ClickRaycast()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;

            Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(rayo.origin, rayo.direction * 50, Color.blue);
            bool choque = Physics.Raycast(rayo, out hit, 50, LayerMask.GetMask("Suelo"));
            if (choque)
            {
                destino = hit.point;
            }
        }
    }

    void Desplazarse()
    {
        origen = transform.position;
        Debug.DrawRay(origen, Direccion[0] * distancia);
        bool impactoDelante = Physics.Raycast(origen, Direccion[0], distancia, LayerMask.GetMask("Pared"));
        Debug.DrawRay(origen, Direccion[1] * distancia);
        bool impactoAtras = Physics.Raycast(origen, Direccion[1], distancia, LayerMask.GetMask("Pared"));
        Debug.DrawRay(origen, Direccion[2] * distancia);
        bool impactoDerecha = Physics.Raycast(origen, Direccion[2], distancia, LayerMask.GetMask("Pared"));
        Debug.DrawRay(origen, Direccion[3] * distancia);
        bool impactoIzquierda = Physics.Raycast(origen, Direccion[3], distancia, LayerMask.GetMask("Pared"));
        if ((Vector3.Distance(transform.position, destino) > 0.001F && !impactoDelante) || (Vector3.Distance(transform.position, destino) > 0.001F && !impactoAtras))
        {
            Vector3 posicion = transform.position;
            Vector3 moveTowards = Vector3.MoveTowards(transform.position, destino, 5 * Time.deltaTime);

            transform.position = new Vector3(moveTowards.x, posicion.y, moveTowards.z);
        }

    }

}
