using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastMovement : MonoBehaviour
{
    Vector3 origen;
    Vector3[] Direccion;
    float distancia=1f;
    // Start is called before the first frame update
    void Start()
    {
        
        Direccion = new Vector3[4];
        Direccion[0] = Vector3.forward;
        Direccion[1] = Vector3.back;
        Direccion[2] = Vector3.right;
        Direccion[3] = Vector3.left;
    }

    // Update is called once per frame
    void Update()
    {
        origen = transform.position;
        Debug.DrawRay(origen, Direccion[0] * distancia);
        bool impactoDelante = Physics.Raycast(origen, Direccion[0], distancia, LayerMask.GetMask("Pared"));
        Debug.DrawRay(origen, Direccion[1] * distancia);
        bool impactoAtras = Physics.Raycast(origen, Direccion[1], distancia, LayerMask.GetMask("Pared"));
        Debug.DrawRay(origen, Direccion[2] * distancia);
        bool impactoDerecha = Physics.Raycast(origen, Direccion[2], distancia, LayerMask.GetMask("Pared"));
        Debug.DrawRay(origen, Direccion[3] * distancia);
        bool impactoIzquierda = Physics.Raycast(origen, Direccion[3], distancia, LayerMask.GetMask("Pared"));

        float InputH = Input.GetAxisRaw("Horizontal");
        float InputV = Input.GetAxisRaw("Vertical");
        if (!impactoDelante && InputV > 0 || !impactoAtras && InputV<0 || !impactoDerecha && InputH > 0 || !impactoIzquierda && InputH< 0)
        {
            transform.Translate((2 * InputH) * Time.deltaTime, 0, (2*InputV)*Time.deltaTime);
        }
      
    }
}
