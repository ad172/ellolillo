using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackMinion : MonoBehaviour
{
    [SerializeField] Side Lado;
    [SerializeField] NavMeshAgent Nose;
    [SerializeField] List<GameObject> Enemigos = new List<GameObject>();
    [SerializeField] float DistanciaInicial = 2f;
    [SerializeField] GameObject NucleoEnemigo;
    [SerializeField] GameObject proyectil;
    bool attacking = false;
    private void Awake()
    {
        switch (Lado)
        {
            case  Side.Red:
                NucleoEnemigo = GameObject.Find("NucleoAzul");
                proyectil.layer = 10;
                break;
            case Side.Blue:
                NucleoEnemigo = GameObject.Find("NucleoRojo");
                proyectil.layer = 11;
                break;
        }
        Nose = GetComponent<NavMeshAgent>();
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != 12)
        {
            GetComponent<Vida>().QuitarVida();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(Enemigos.Count > 0) { 
        if (Vector3.Distance(transform.position,Enemigos[0].transform.position) <= DistanciaInicial )
        {
            
            Vector3 posicionEnemigo = new Vector3(Enemigos[0].transform.position.z - DistanciaInicial, Enemigos[0].transform.position.y, Enemigos[0].transform.position.x - DistanciaInicial);
            Nose.SetDestination(posicionEnemigo);
            }
            else
            {
                if (!attacking) {
                   
                StartCoroutine(AttackEnemyMinion());
                    
                }
                Nose.isStopped = true;
            }
        }
        else
        {
            StopCoroutine(AttackEnemyMinion());
            Nose.isStopped = false;
            Nose.SetDestination(NucleoEnemigo.transform.position);
        }
    }
    
   IEnumerator AttackEnemyMinion()
    {
        while (true) { 
        attacking = true;
        
        GameObject proyecti= Instantiate(proyectil,transform.position,transform.rotation);
            proyecti.transform.position = gameObject.transform.position;
        proyecti.GetComponent<movementProyectil>().AsignarObjetivo(Enemigos[0].transform.position);
        switch (Lado)
        {
            case Side.Red:
                
                proyecti.layer = 10;
                break;
            case Side.Blue:
                
                proyecti.layer = 11;
                break;
        }
        yield return new WaitForSeconds(1f);
        attacking = false;
        }
    }
    public void AddEnemigo(GameObject a)
    {
        Enemigos.Add(a);
    }
   public void EnemyDie(GameObject a)
    {
        Enemigos.Remove(a);
    }
}
