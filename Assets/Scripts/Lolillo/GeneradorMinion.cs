using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GeneradorMinion : MonoBehaviour
{
    [SerializeField] GameObject Linea;
    [SerializeField] GameObject origen;
    [SerializeField] GameObject Minion;
    [SerializeField] float tiempo = 30f;

    [SerializeField] int CantidadDeMinions = 4;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Minions());
    }

    // Update is called once per frame
    void Update()
    {
     
    }

    IEnumerator Minions()
    {
        while (true)
        {
            
                for (int b = 0; b<CantidadDeMinions; b++) { 
                    GameObject minion = Instantiate(Minion, origen.transform.position, transform.rotation);
                    minion.name = "Minion" + b.ToString();
                    minion.GetComponent<NavMeshAgent>().SetDestination(Linea.transform.position);
                    yield return new WaitForSeconds(0.5f);
                }
            yield return new WaitForSeconds(tiempo);
        }
    }
}
