using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectarEnemigo : MonoBehaviour
{
    [SerializeField] Side Lado;
    [SerializeField] AttackMinion attack;

    private void OnEnable()
    {
        Vida.OnDiying += minionDead; 
    }
    private void OnDisable()
    {
        Vida.OnDiying -= minionDead;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(Lado == Side.Red && other.gameObject.layer == 9)
        {
            attack.AddEnemigo(other.gameObject);
            
        }
        if (Lado == Side.Blue && other.gameObject.layer == 8)
        {
            attack.AddEnemigo(other.gameObject);
           
        }
    }
    void minionDead(Vida vida)
    {
        attack.EnemyDie(vida.gameObject);
    }
    private void OnTriggerExit(Collider other)
    {
        if (Lado == Side.Red && other.gameObject.layer == 9)
        {
            attack.EnemyDie(other.gameObject);

        }
        if (Lado == Side.Blue && other.gameObject.layer == 8)
        {
            attack.EnemyDie(other.gameObject);

        }
    }
}
