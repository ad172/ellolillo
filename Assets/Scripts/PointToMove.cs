using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class PointToMove : MonoBehaviour
{
   [SerializeField] NavMeshAgent player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ClickRaycast();
    }
    void ClickRaycast()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;

            Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(rayo.origin, rayo.direction * 50, Color.blue);
            bool choque = Physics.Raycast(rayo, out hit, 50, LayerMask.GetMask("Suelo"));
            if (choque)
            {
                player.SetDestination(hit.point);
            }
        }
    }
}
